/**
 * @description 
 * @author Gil Gourévitch
 * @date 
 * @see 
 * 
 */
public with sharing class MeetupClass2 {

	/**
	 * @description Contructor
	 */
	public MeetupClass2() {
		
	}

	/**
	 * @description Sample method
	 *
	 * @param param
	 * @return nothing
	 */
	public void myMethod(Object param){
		system.debug('MeetupClass2.myMethod');
	}
}